# -*- coding: utf-8 -*-
import sublime
import sublime_plugin
import textwrap
import re
import sys
import os
import webbrowser
from .html_redmine import *

REDMINE_HOST = "red.eltex.loc"
reg_exprs=['/\*.+:.*#\d+,.*.+\*/', '//.+:.*#\d+,.*.+']
issue_symbols=['#', '#']
#/* Kogdin: #95077, 20.03.2018 */
# // Skvortsov: #16839, 27.12.13; add OMCI bridge MC support
#view.run_command('sublimeredminecomments')
class sublimeredminecomments(sublime_plugin.TextCommand):
	def popupRedmine(self, issue_id):
		redmine = Redmine();
		#print("id =" + issue_id+ "|")
		redmine.setIssue(issue_id);
		title = redmine.getTitle();
		#print(title)
		link = redmine.getLink();
		#print(link)
		desc = redmine.getDescription();
		status = redmine.getStatus();
		#print(status)
		version = redmine.getVersion();
		commits = redmine.getCommits();
		hrefs = redmine.getCommitHrefs();
		times = redmine.getCommitTimes();
		#print("commits:")
		#print(commits)
		#print("hrefs:")
		#print(hrefs)
		commitsBuf=""
		for com in commits:
			commitsBuf=commitsBuf+"<a href=\"%s\">%s</a> %s<br>"%(hrefs[commits.index(com)],com, times[commits.index(com)])
		#print(commitsBuf)

		#print(desc)
		#self.view.insert(edit, pt, "Hello, World!")
		s = \
		"<html>"\
		'<style>'\
        'html, body {'\
        '    width: 1000;'\
        '    min-height: 200;'\
        '    max-height: 2000;'\
        '    background-color: black;'\
        '}'\
        '</style>'\
        '<body>' + \
		"<a href=\"%s\">Ссылка на задачу</a>" \
		"<br>%s" \
		"<br>Статус:%s" \
		"<br>Версия:%s" \
		"<br>Ревизии:<br>%s" \
		"<br>Описание задачи:" \
		"<br>%s"%(link, title,status, version,commitsBuf, desc) + \
		'</body>'\
		'</html>'
		#print(s)
		self.view.show_popup(s, on_navigate=self.openLink, max_width=1200, max_height=200)

	def run(self, edit, event=None):
		pt = 0
		selectedText = self.view.substr(self.view.line(self.view.sel()[0]))
		#print(selectedText)
		for reg in reg_exprs:
			regex = re.compile(reg) 
			mo = regex.search(selectedText)
			if mo:
				tmp=mo.group(0)
				tmp2 = tmp[tmp.index('#')+len('#'):]
				issue_id = tmp2[:tmp2.index(',')]
				self.popupRedmine(issue_id)
			

	def openLink(self, url):
		webbrowser.open_new_tab(url)

	def want_event(self):
		return True