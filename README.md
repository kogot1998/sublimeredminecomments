Скрипт создает всплывающее окно при взаимодействии с комментарием о задаче
  
Способ взаимодействия в файле Default.sublime-mousemap, по умолчанию ctrl + 2 x ПКМ
  
Если неудобно с мышкой, то можно добавить команду на клавиатуру: Preferences->Key Bindings
  
И создай здесь пользовательскую команду типа { "keys": ["alt+v"], "command": "sublimeredminecomments"}
  
Для взаимодействия нужно чтобы курсор был установлен на комментарий.
  
Установка:
  
cd ~/.config/sublime-text-3/Packages
  
git clone https://kogot1998@bitbucket.org/kogot1998/sublimeredminecomments.git
