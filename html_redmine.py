#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from . import httplib2
import urllib

REDMINE_HOST = "red.eltex.loc"
class Redmine():
    def __init__(self):
        """Constructor"""
        self.issue_id = "-1"
        self.issue_title = ""
        self.issue_desc = ""
        self.issue_href = ""
        self.issue_status = ""
        self.issue_version = ""
        self.issue_commits = []
        self.issue_commit_hrefs = []
        self.issue_commit_times = []

    #def checkLink(self ,link):
    #    ''' Docstring '''
    #    try:
    #        #print("try check link:"+link)
    #        http = httplib2.Http()
    #        headers = {'Content-Type':'application/x-www-form-urlencoded'}
    #        response, content = http.request(link,'GET', headers=headers)
    #        return response['status'];
    #    except:
    #        raise

    def setIssueTitle(self ,content):
        ''' Docstring '''
        try:
            toFind='<link rel="alternate"'
            tmp = content[content.index(toFind)+len(toFind):]
            tmp2 = tmp[tmp.index('title="') + len('title="'):]
            ret = (tmp2[:tmp2.index('" href="')])
            return ret;
        except:
            raise

    def setIssueVersion(self ,content):
        ''' Docstring '''
        try:
            toFind='fixed-version attribute'
            tmp = content[content.index(toFind)+len(toFind):]
            #print("tmp " +tmp)
            tmp2 = tmp[tmp.index('<div class="value">') + len('<div class="value">'):]
            #print("tmp2 " +"%d"%(tmp.index('href=')))
            tmp3 = tmp2[tmp2.index('R') + len('R'):]
            #print("tmp3 " +"%d"%(tmp2.index('R') ))
            ret = (tmp3[:tmp3.index('</div>') if tmp3.index('</div>') < tmp3.index('</a>') else tmp3.index('</a>')])
            return ret;
        except:
            raise

    def setIssueStatus(self ,content):
        ''' Docstring '''
        try:
            toFind='<div class="status attribute">'
            tmp = content[content.index(toFind)+len(toFind):]
            tmp2 = tmp[tmp.index('<div class="value">') + len('<div class="value">'):]
            ret = (tmp2[:tmp2.index('</div>')])
            return ret;
        except:
            raise

    def setIssueCommits(self ,content):
        ''' Docstring '''
        try:
            commits=[] 
            hrefs=[]
            times=[]
            toFind='Ревизия '
            #toFind2='Редакция '
            tmp = content
            while toFind in tmp:
                tmp = tmp[tmp.index(toFind)+len(toFind):]
                commits.append(tmp[:tmp.index('"')])
                href_1 = tmp[tmp.index('href="')+len('href="'):]
                hrefs.append('http://' + REDMINE_HOST + '/' + href_1[:href_1.index('"')])
                time = tmp[tmp.index('<a title="') + len('<a title="'):]
                times.append(time[:time.index('"')])
                #commits.append(tmp[tmp.index('">')+len('">'):tmp.index('</')])
            #tmp2 = tmp[tmp.index('<div class="value">') + len('<div class="value">'):]
            #ret = (tmp2[:tmp2.index('</div>')])
            return commits, hrefs, times;
        except:
            raise

    def setIssueDescription(self ,content):
        ''' Docstring '''
        try: 
            toFind='<div class="wiki">'
            start = content[content.index(toFind)+len(toFind):]
            end = 0
            tmp = start
            count = 1
            while(1):
                #if '</div>' not in tmp and '<div' not in tmp:
                #    break;
                #print(tmp)
                if '<div' in tmp:
                    idx1 = tmp.index('<div')
                else:
                    idx1 = -1;
                if '</div>' in tmp:
                    idx2 = tmp.index('</div>')
                else:
                    print("strange error")
                    print(tmp)
                    ret = "Read page error"
                    break;
                #print("idx1 = %d idx2 = %d"%(idx1,idx2))
                if idx1 < idx2:
                    print('Find <div')
                    end = end + idx1 + len('<div')
                    count = count + 1;
                    tmp = tmp[idx1+len('<div'):]
                else:
                    #print('Find </div')
                    count = count - 1;
                    if count == 0:
                        end = end + idx2
                        ret = start[:end]
                        break;
                    end = end + idx2 +len('</div>')
                    tmp = tmp[idx2+len('</div>'):]
            return ret;
        except:
            raise

    def getTitle(self):
        return self.issue_title;

    def getDescription(self):
        return self.issue_desc;

    def getStatus(self):
        return self.issue_status;

    def getLink(self):
        return self.issue_href;

    def getVersion(self):
        return self.issue_version;

    def getCommits(self):
        return self.issue_commits;

    def getCommitHrefs(self):
        return self.issue_commit_hrefs;

    def getCommitTimes(self):
        return self.issue_commit_times;

    def setIssue(self, issueid):
        self.issue_href = "http://" + REDMINE_HOST + "/issues/%s"%(issueid);
        #print("href"+self.issue_href)
        http = httplib2.Http()
        headers = {'Content-Type':'application/x-www-form-urlencoded'}
        response, content = http.request(self.issue_href,'GET', headers=headers)
        if response['status'] != '200':
            return 0;
        # &#39 to ' and #39 to '
        str_content=content.decode('utf-8')
        str_content=str_content.replace('&#39;', '\'')
        str_content=str_content.replace('#39;', '\'')
        self.issue_id = issueid;
        self.issue_title = self.setIssueTitle(str_content)
        self.issue_desc = self.setIssueDescription(str_content)
        self.issue_status = self.setIssueStatus(str_content)
        self.issue_version = self.setIssueVersion(str_content)
        self.issue_commits, self.issue_commit_hrefs, self.issue_commit_times = self.setIssueCommits(str_content)